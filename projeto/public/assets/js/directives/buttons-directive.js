angular.module("ngWpensarButtonsDirectives", [])
    .directive("buttons-box", function () {

		return {
            restrict: "E",
            templateUrl: "assets/js/directives/html/button-incluir.html",
            transclude: true
        }

	})
	.directive("buttonRetornar", function () {

		return {
            restrict: "E",
            templateUrl: "assets/js/directives/html/button-retornar.html",
            scope: {
                link: "@"
            }
        }

	})
    .directive("buttonIncluir", function () {

		return {
            restrict: "E",
            templateUrl: "assets/js/directives/html/button-incluir.html",
            scope: {
                link: "@"
            }
        }

	});
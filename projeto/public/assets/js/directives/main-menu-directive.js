angular.module("ngWpensarMainMenuDirectives", [])
    .directive("mainMenuBox", function () {

		return {
            restrict: "E",
            templateUrl: "assets/js/directives/html/main-menu-box.html",
            transclude: true
        }

	})
	.directive("mainMenuPart", function () {

		return {
            restrict: "E",
            templateUrl: "assets/js/directives/html/main-menu-part.html",
            scope: {
                link: "@",
                name: "@"
            }
        }

	});
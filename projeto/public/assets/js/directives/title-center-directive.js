angular.module("ngWpensarDomDirectives", [])
	.directive("titleCenter", function () {

		return {
            restrict: "E",
            templateUrl: "assets/js/directives/html/title-center.html",
            scope: {
                titulo: "@"
            }
        }

	});
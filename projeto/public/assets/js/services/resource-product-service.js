angular.module("ngWpensarProductsServices", ["ngResource"])
	.factory("$resourceProducts", function ($resource) {

		return $resource("http://private-929173-querotrabalharnawpensar.apiary-mock.com/api/produtos/:id", null, {
			update: {
				method: "put"
			}
		});
		
	});
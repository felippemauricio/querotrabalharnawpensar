angular.module("ngWpensarDateServices", ["ngResource"])
	.service('$date', function ($filter) {
        
        this.todayNow = function () {
            var data = new Date();
            var dia = data.getDate();
            var mes = data.getMonth() + 1;
            var ano = data.getFullYear();
            return [dia, mes, ano].join('/');
        }
        
        this.formatDate = function (date) {
            return $filter('date')(date, "dd/MM/yyyy HH:mm:ss");
        }
        
        return this;
        
    });
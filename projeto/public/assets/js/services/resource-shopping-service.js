angular.module("ngWpensarShoppingServices", ["ngResource"])
	.factory("$resourceShopping", function ($resource) {

		return $resource("http://private-85ad8-querotrabalharnawpensar.apiary-mock.com/api/compras/:id", null, {
			update: {
				method: "put"
			}
		});
		
	});
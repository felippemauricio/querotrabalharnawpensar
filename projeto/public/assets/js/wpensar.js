angular.module("wpensar", ["ngRoute", "ngWpensarButtonsDirectives", "ngWpensarDateServices", "ngWpensarDomDirectives", "ngWpensarMainMenuDirectives", "ngWpensarProductsServices", "ngWpensarShoppingServices"])
	.config(function ($locationProvider, $routeProvider) {

		$locationProvider.html5Mode(true);

		$routeProvider.when("/main-menu", {
			templateUrl: "partial/main-menu.html"
		})
		.when("/products", {
			templateUrl: "partial/products.html",
            controller: "ProductsController"
		})   
        .when("/product/new", {
			templateUrl: "partial/product.html",
			controller: "ProductController"
		})
		.when("/product/edit/:id", {
			templateUrl: "partial/product.html",
			controller: "ProductController"
		}) 
        .when("/shopping", {
			templateUrl: "partial/shopping.html",
			controller: "ShoppingController"
		})
        .when("/shop/new", {
			templateUrl: "partial/shop.html",
			controller: "ShopController"
		})
		.when("/shop/edit/:id", {
			templateUrl: "partial/shop.html",
			controller: "ShopController"
		})        
		.otherwise({
			redirectTo: "/main-menu"
		});

	});
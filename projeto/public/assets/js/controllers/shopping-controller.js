angular.module("wpensar").controller("ShoppingController", function ($location, $resourceShopping, $routeParams, $scope) {

    $scope.shopping = []; 

    getShopping = function () {
		$resourceShopping.query(function(shopping) {
			$scope.shopping = shopping;
		}, function (error) {
            console.log("Ocorreu um erro em ShoppingController -> getShopping");
			console.log(error);
		});	
	}
    
    $scope.redirShop = function (shop) {
        var uri;
        uri = "shop/edit/" + shop.id;
        $location.path(uri);
    }

    init = function () {
        getShopping();
    }
    
    init();

});
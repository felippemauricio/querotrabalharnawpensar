angular.module("wpensar").controller("ProductsController", function ($location, $resourceProducts, $routeParams, $scope) {

    $scope.products = []; 

    getProducts = function () {
		$resourceProducts.query(function(products) {
			$scope.products = products;
		}, function (error) {
            console.log("Ocorreu um erro em ProductsController -> getProducts");
			console.log(error);
		});	
	}
    
    $scope.redirProduct = function (product) {
        var uri;
        uri = "product/edit/" + product.id;
        $location.path(uri);
    }

    init = function () {
        getProducts();
    }
    
    init();

});
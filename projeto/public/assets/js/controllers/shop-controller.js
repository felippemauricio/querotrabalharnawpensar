angular.module("wpensar").controller("ShopController", function ($date, $location, $resourceShopping, $routeParams, $scope) {

    /* OBS:
        Tentei ler todos os produtos e montar um select com todos os produtos usando ng-options.
        A montagem do componente funcionou, porém o codigo do produto cadastrado nos webservices de compras
        nunca batiam com o que eu lia dos produtos o que impossibilitou a continuacao do mesmo.
    */

    $scope.shop         = {};
    $scope.titlePartial = "";
    $scope.newShop      = false;
    $scope.message      = "";
    $scope.products     = [];

    getIdShop = function () {
        return $routeParams.id;
    }

    isEditShop = function () {
        return getIdShop ();
    }
    
    seeShop = (function () {
        $scope.titlePartial = "Visualizando a Compra";
        $scope.newShop      = false;
    });
    
    getShop = function () {
        var request;
        request = { 
            id: getIdShop()
        };
		$resourceShopping.get(request, function(shop) {
			$scope.shop      = shop;
            $scope.shop.data = $date.formatDate($scope.shop.data);
		}, function (error) {
			console.log("Ocorreu um erro em ShopController -> getShop");
			console.log(error);
		});
	}    
    
    $scope.sendShop = function () {
        var request;
        request = { 
            nome: $scope.shop.nome,
            quantidade: $scope.shop.quantidade,
            preco: $scope.shop.preco,
            data: $date.todayNow(),
        };
		$resourceShopping.save(request, function(shop) {
			$scope.shop      = shop; // OBS: Resposta do webservice é sempre fixa
            $scope.shop.data = $date.formatDate($scope.shop.data);
            $scope.message   = "Compra foi incluída com sucesso!";
            seeShop();
		}, function (error) {
			console.log("Ocorreu um erro em ShopController -> sendShop");
			console.log(error);
		});
    }
    
    $scope.deleteShop = function () {
        var request;
        request = { 
            id: $scope.shop.id
        };
		$resourceShopping.delete(request, function(shop) {
			$location.path("/shopping");
		}, function (error) {
			console.log("Ocorreu um erro em ShopController -> deleteShop");
			console.log(error);
		});
    }

    init = function () {
        if (isEditShop()) {
            seeShop();
            getShop();
        }
        else {
            $scope.titlePartial = "Incluindo a compra";
            $scope.newShop = true;
        }
    }
    
    init();
    
});
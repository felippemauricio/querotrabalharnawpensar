angular.module("wpensar").controller("ProductController", function ($location, $resourceProducts, $routeParams, $scope) {

    $scope.product      = {};
    $scope.titlePartial = "";
    $scope.newProduct   = false;
    $scope.message      = "";

    getIdProduct = function () {
        return $routeParams.id;
    }

    isEditProduct = function () {
        return getIdProduct ();
    }
    
    seeProduct = (function () {
        $scope.titlePartial = "Visualizando o Produto";
        $scope.newProduct = false;
    });
    
    getProduct = function () {
        var request;
        request = { 
            id: getIdProduct()
        };
		$resourceProducts.get(request, function(product) {
			$scope.product = product;
		}, function (error) {
			console.log("Ocorreu um erro em ProductController -> getProduct");
			console.log(error);
		});
	}    
    
    $scope.sendProduct = function () {
        var request;
        request = { 
            nome: $scope.product.nome
        };
		$resourceProducts.save(request, function(product) {
			$scope.product = product; // OBS: Resposta do webservice é sempre fixa
            $scope.message = "Produto foi incluído com sucesso!";
            seeProduct();
		}, function (error) {
			console.log("Ocorreu um erro em ProductController -> sendProduct");
			console.log(error);
		});
    }
    
    $scope.deleteProduct = function () {
        var request;
        request = { 
            id: $scope.product.id
        };
		$resourceProducts.delete(request, function(product) {
			$location.path("/products");
		}, function (error) {
			console.log("Ocorreu um erro em ProductController -> deleteProduct");
			console.log(error);
		});
    }

    init = function () {
        if (isEditProduct()) {
            seeProduct();
            getProduct();
        }
        else {
            $scope.titlePartial = "Incluindo o Produto";
            $scope.newProduct = true;
        }
    }
    
    init();

});
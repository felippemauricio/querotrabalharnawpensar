# **Felippe Maurício Vieira** #

# **Organização do Projeto** #

* Existem 2 pastas: curriculo(Onde se encontra um PDF do meu currículo) e Projeto(Onde se encontra a minha solução para o Projeto Front-End em AngularJS);

# **Como configuro o ambiente para execução do projeto?** #

1. Instale o NodeJS no seu Linux (https://www.digitalocean.com/community/tutorials/como-instalar-o-node-js-em-um-servidor-ubuntu-14-04-pt) ou OS X (http://coolestguidesontheplanet.com/installing-node-js-on-osx-10-10-yosemite/);
2. Pelo terminal, entre na pasta do projeto e rode o seguinte comando: npm start;
3. Abra o browser de sua preferência (Chrome, Firefox, Safari) e digite na URL: http://localhost:3000;

## ** Por que me candidato a está vaga? ** ##

1. Porque amo desafios novos;
2. Estou em busca de um local de trabalho onde eu possa crescer;
3. Tenho vontade de um dia abrir a minha startup e gostaria de conhecer o ambiente de uma;
4. Gostaria de ter contato com pessoas criativas e que possam me acrescentar algo todos os dias;
5. Gostaria de acrescentar algo a essas pessoas;
6. Gostaria de poder mostrar todo o meu potencial;

## ** Por que optei pelo desafio Front-End? ** ##

* Optei pelo desafio Front-End pelo simples fato de me sentir mais a vontade. Amo trabalhar com JavaScript e por causa disso, optei por este caminho. Na verdade, foi uma dúvida cruel, pois acabei de fazer o curso do Henrique Bastos (Welcome To The Django) e estava super afim de ter a oportunidade de trabalhar com Python, mas como nunca fiz algo em produção, optei pelo o que eu já tenho mais costume.
